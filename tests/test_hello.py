import pytest
from tutorial.hello import hello


def test_hello():
    assert hello('Manfred') == 'Hello Manfred'

.. python-tutorial documentation master file, created by
   sphinx-quickstart on Thu Apr 23 16:04:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to python-tutorial's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./README.rst

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

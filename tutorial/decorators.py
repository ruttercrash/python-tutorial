from functools import wraps


def my_decorator(function):
    @wraps(function)
    def wrapper(*args):
        print("Before")
        result = function(*args)
        print("After")
        return result

    return wrapper


def say_hi(name: str) -> str:
    """
    Greets with name

    :param name: name to greet
    :returns: 'success' or 'failure'
    """

    print(f"Hey {name}!")
    return "Success"

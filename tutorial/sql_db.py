"""Show how to map python objects to sql tables"""


from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


ENGINE = create_engine("sqlite:///:memory:", echo=True)
Session = sessionmaker(bind=ENGINE)
session = Session()
Base = declarative_base()


class User(Base):
    """User object to store in table users"""

    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)

    def __repr__(self):
        return f"User {self.name}"


Base.metadata.create_all(ENGINE)

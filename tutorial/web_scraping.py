#!/usr/bin/env python3


import requests
from bs4 import BeautifulSoup


response = requests.get("https://releases.hashicorp.com/vagrant/")
html = response.text
soup = BeautifulSoup(html, "lxml")
all_links = soup.find_all("a")
current_vagrant_version_number = all_links[1].text.split("_")[1]

print(current_vagrant_version_number)

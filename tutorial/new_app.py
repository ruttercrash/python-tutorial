#!/usr/bin/env python3


from argparse import ArgumentParser
import os
import re
from subprocess import run, CalledProcessError
from sys import exit


APP_NAME_REGEX = r"^\w+(_?\w+)*$"  # only letters & underscores
DOMAIN_REGEX = r"^([\w\d]+\.)+([a-z][a-z]+)+$"  # sub.domain.tld


def check_run(command, error_message):
    try:
        run(command, shell=True, check=True)
    except CalledProcessError:
        exit(error_message)


parser = ArgumentParser(description="Add nginx server block for proxy pass from subdomain to local port", add_help=True)
parser.add_argument("app_name", action="store", help="Name for nginx conf file")
parser.add_argument("domain", action="store", help="(Sub-)domain for the app")
parser.add_argument("port", action="store", type=int, help="Local port for nginx proxy pass")
parser.add_argument("--email", action="store", dest="email", help="Certbot email (needed only on 1st run)")
args = parser.parse_args()

if os.geteuid() != 0:
    exit("ERROR: Please run as root or use sudo.")

if not re.search(APP_NAME_REGEX, args.app_name):
    exit("ERROR: App name must have only letters and underscores.")

if not re.search(DOMAIN_REGEX, args.domain):
    exit("ERROR: Invalid url format. Use (subdomain.)domain.toplevel")

if args.port < 1024:
    exit("ERROR: Please use an unprivileged port between 1024 - 65535.")

nginx_path = f"/etc/nginx/conf.d/{args.app_name}.conf"
nginx_conf = f"""server {{
    server_name   {args.domain};

    location / {{
        proxy_pass  http://127.0.0.1:{args.port};
    }}
}}
"""
print(nginx_conf)
write_config = input(f"Write to {nginx_path} (y/n)?")
if write_config != "y":
    exit("No config file written.")

with open(nginx_path, "w") as nginx_file:
    nginx_file.write(nginx_conf)
print("Config file written.\n")

certbot_cmd = f"/usr/bin/certbot --nginx --non-interactive --agree-tos --domains {args.domain} "
if args.email:
    certbot_cmd += f"--email {args.email}"

check_run("systemctl restart nginx.service", "ERROR: Failed to restart nginx.")
check_run(certbot_cmd, "ERROR: Certbot failed to get SSL-cert.")

print("\n\u001b[32mSUCCESS: Nginx config updated & SLL-cert obtained.")

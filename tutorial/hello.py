""" Hello world best practise """


def hello(name: str) -> str:
    """
    Greet someone with name

    :param name: his or her name
    :return: Greeting message

    :Example:
        >>> from hello import hello
        >>> hello('Ansgar')
        'Hello Ansgar'
    """
    return f"Hello {name}"

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # special method aka. double underscore or "dunder" method
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

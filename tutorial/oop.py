class Tier:
    art = "säugetier"

    @classmethod
    def leg_eier(cls):
        if Tier.art == "säugetier":
            return f"Kann ich nicht, bin ein {Tier.art}"
        elif Tier.art == "fisch":
            return "Brüte..."

    @staticmethod
    def statische_methode():
        return "Ich kann Tier.art nicht sehen :("


class Hund(Tier):
    def __init__(self, name):
        self.name = name
        self.hungrig = False

    def friss(self, futter):
        if futter == "hundefutter":
            self.hungrig = False
            return "hmmm"
        return "bäääh"

    def hols(self, gegenstand):
        if not self.hungrig:
            self.hungrig = True
            return f"Hole {gegenstand}!"
        return "HUNGER!!!"

    @classmethod
    def was_bin_ich(cls):
        return f"Ich bin ein {Tier.art}"


class Dogge(Hund):
    def belle(self):
        return "Wuff"


class Katze:
    def miau(self):
        return "miau"


class Katzenhund(Dogge, Katze):
    pass

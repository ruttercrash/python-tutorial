def my_deco(func):
    def wrapper():
        result = func()
        result += 1
        return result

    return wrapper


def func():
    return 42

#!/usr/bin/env python3


from argparse import ArgumentParser


parser = ArgumentParser()

parser.add_argument('number', action='store', type=str)
parser.add_argument('unit', action='store')

args = parser.parse_args()

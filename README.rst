"""""""""""""""
Python Tutorial
"""""""""""""""

TL;DR 	This aims to be a setup of a python project following best pratices.

We set up a fresh virtualenv and activate it. Project and development dependencies are kept in separate files. Project files are kept in a python package (here: tutorial directory). Testsuite ist located in dir tests, Documentation in docs dir. Docs are auto-generated from docstrings in the source code. Examples in the docstrings can be tested with doctest before building the docs. We run a test suite against multiple python versions with coverage stats and report of missing lines. On-the-fly typechecking in Pycharm and format code on save.

.....
Usage
.....

Install dependencies and development dependencies into virtualenv:

.. code:: bash

    python -m venv tutorial-env
    source tutorial-env/bin/activate
    pip install --upgrade pip

    pip install -r requirements.txt
    pip install -r requirements-dev.txt

Run tutorial examples, e. g.:

.. code:: bash

    cd tutorial
    python cmd_args.py -h

Play with examples in a python shell:

.. code:: python

    In [1]: %run cmd_args.py 1 cm 
    In [2]: args.number
    Out[2]: '1'
    
    In [3]: args.unit
    Out[3]: 'cm'

Run testsuite with coverage and report missing lines:

.. code:: bash

    pytest --cov=tutorial --cov-report term-missing tests

Run testsuite against multiple Python versions:

.. code:: bash

   tox

Build and read API docs in your browser:

.. code:: bash

    cd docs
    make html 
    firefox _build/html/index.html


